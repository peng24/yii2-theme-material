<?php

/*
 * 2015-07-14
 * https://github.com/FezVrasta/bootstrap-material-design
 * http://www.rid7.com
 */

use yii\web\AssetBundle;

class MaterialAsset extends AssetBundle {

    public $sourcePath = '@vendor/peng24/yii2them-material/assets';
    public $baseUrl = '@web';
    public $css = [
        'css/material-fullpalette.min.css',
        'css/material.min.css',
        'css/ripples.min.css',
    ];
    public $js = [
        'js/material.min.js',
        'js/ripples.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public function init() {
        parent::init();
    }

}
